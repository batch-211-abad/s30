// Use the count operator to count the total number of fruits on sale

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$count: "fruitsOnSale"}
]);


// Use the count operator to count the total number of fruits with stock more than 20

db.fruits.aggregate([
	{$match: {stock: {$gt: 20}}},
	{$count: "enoughStock"}
]);


// Use the average operator to get the average price of fruits on sale per supplier

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", average_price: {$avg: "$price"}}},
]);


// Use the max operator to get the highest price of a fruit per supplier

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", maximum_price: {$max: "$price"}}},
]);


// Use the min operator to get the lowest price of a fruit per supplier. 

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", minimum_price: {$min: "$price"}}},
]);




